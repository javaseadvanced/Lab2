
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MemoryDaemon implements Runnable {
	private long memoryUsed = 0;
	public MemoryDaemon() {
		
	}

	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;

		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != getMemoryUsed()) {
				System.out.println("\tMemory used = " + used);
				setMemoryUsed(used);
				Path path = Paths.get("/tmp/mdaemon.log");
				try(BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("UTF-8"))){
					writer.write("Memory used = " + used + "\n");
					}catch(IOException ex){
						ex.printStackTrace();
						}
			}
		}
	}

	public long getMemoryUsed() {
		return memoryUsed;
		
	}

	public void setMemoryUsed(long memoryUsed) {
		this.memoryUsed = memoryUsed;
		
		}
}
