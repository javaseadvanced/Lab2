
public class OrderSaver {

	public OrderSaver() {
		
	}
	public static void saveOrder(String fileName, Order order1) {
		try {
			// Create a file to store the Order object in
			java.io.File objectFile = new java.io.File(fileName);
			if (objectFile.exists()) {
				objectFile.delete();
				}
			// Create the output stream to write out the file
			java.io.FileOutputStream fos = new java.io.FileOutputStream(objectFile);
			java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fos);
			// Write the Order object out to the file
			oos.writeObject(order1);
			oos.close();
			} catch (Exception e) {
				e.printStackTrace();
				}
		}
	public static Order restoreOrder(String fileName) {
		try {
			// Create a file to store the Order object in
			java.io.File objectFile = new java.io.File(fileName);
			
			// Create the input stream to write out the file
			java.io.FileInputStream fis = new java.io.FileInputStream(objectFile);
			java.io.ObjectInputStream ois = new java.io.ObjectInputStream(fis);
			
			Order retrieved = (Order) ois.readObject();
			ois.close();
			
			return retrieved;
			} catch (Exception e) {
				e.printStackTrace();
				}
		return null;
		}
	}