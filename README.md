# Lab2
Лямбда-выражения

#Содержание файла "LTester.java"

public class LTester {

	public static void main(String[] args) {

	}

}


#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "LTester build and run",
            "command": "javac -sourcepath /projects/Lab2/LTester/src -d /projects/Lab2/LTester/bin /projects/Lab2/LTester/src/*.java && java -classpath /projects/Lab2/LTester/bin LTester",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab2/LTester/src",
                "component": "maven"
            }
        }
    ]
}
